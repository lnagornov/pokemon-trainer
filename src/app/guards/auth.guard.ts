import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from "../services/user.service";


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // Redirect User if it does not exist
    if (this.userService.user) {
      return true;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }
}
