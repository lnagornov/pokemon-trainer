import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { LoginService } from "../../services/login.service";
import { UserService } from "../../services/user.service";
import { User } from "../../models/user.model";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  get user(): User | undefined {
    return this.userService.user;
  }

  constructor(
    private readonly router: Router,
    private readonly loginService: LoginService,
    private readonly userService: UserService,
  ) { }

  public handleLogout(): void {
    this.loginService.logout();
    this.router.navigateByUrl("/landing");
  }
}
