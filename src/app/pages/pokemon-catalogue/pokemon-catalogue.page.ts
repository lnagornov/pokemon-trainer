import { Component, OnInit } from '@angular/core';

import { PokemonCatalogueService } from "../../services/pokemon-catalogue.service";
import { Pokemon } from "../../models/pokemon.model";


@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemon(): Pokemon[] {
    return this.pokemonService.pokemon;
  }
  get error(): string {
    return this.pokemonService.error;
  }
  get loading(): boolean {
    return this.pokemonService.loading;
  }

  constructor(
    private readonly pokemonService: PokemonCatalogueService,
  ) { }

  ngOnInit(): void {
    this.pokemonService.getAllPokemon();
  }
}
