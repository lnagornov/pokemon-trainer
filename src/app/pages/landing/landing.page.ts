import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { UserService}  from "../../services/user.service";
import { User } from "../../models/user.model";


@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage {

  get user(): User | undefined {
    return this.userService.user;
  }

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  public handleLogin(): void {
    this.router.navigateByUrl("/pokemon-catalogue");
  }
}
