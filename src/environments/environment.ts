// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_USER: "https://ln-noroff-assignment-api.herokuapp.com/trainers",
  API_SPRITES: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon",
  API_POKEMON: "https://pokeapi.co/api/v2/pokemon/",
  API_KEY: "7CC60A6C73EA6BFB0AAB235615D9FF62226FCBBCA22A16D7763ADC2838539AC4",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
