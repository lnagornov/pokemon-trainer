<div align="center">
    <h1>Noroff Assignment 3: Frontend Development with Angular</h1>
    <img src="https://i.ibb.co/GcFNsJz/Angular-full-color-logo.png" width="128" alt="Angular">
</div>

[![license](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Pokémon Trainer

This is the Pokémon Trainer web app using the Angular Framework.

The application allows collecting Pokémon received from the [PokeAPI](https://pokeapi.co) and sprites from [here](https://github.com/PokeAPI/sprites).

Deployed with heroku [here](https://fathomless-beyond-75411.herokuapp.com).

## General info and usage
This front-end project contains the following pages:
* Landing
* Trainer
* Pokémon catalogue

Usage:
* Firstly you must enter username before being able to collect any Pokémon.
* Pick Pokémon you like the most.
* Then you can find your collected Pokémon on your trainer page.

## Technologies
Project is created with:
* HTML
* CSS
* Angular

## Setup
To run this project locally, download it and open the project folder, then run this command.
```bash
ng serve --open
```

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Author
Lev Nagornov

---
2022 Made for Noroff JS course
